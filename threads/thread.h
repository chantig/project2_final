#ifndef THREADS_THREAD_H
#define THREADS_THREAD_H

/////////////////////////////////////////////////////////////////////////////////////////Start
#ifndef USERPROG
#define USERPROG
#endif
/////////////////////////////////////////////////////////////////////////////////////////End

#include <debug.h>
#include <list.h>
#include <stdint.h>
/////////////////////////////////////////////////////////////////////////////////////////Start
#include "synch.h"
#include "filesys/file.h"
#include "userprog/fdstruct.h"
#ifdef VM
#include <hash.h>
#include "filesys/filesys.h"
#define MAX_OPEN_FILES  10
#endif
/////////////////////////////////////////////////////////////////////////////////////////End

/* States in a thread's life cycle. */
enum thread_status
  {
    THREAD_RUNNING,     /* Running thread. */
    THREAD_READY,       /* Not running but ready to run. */
    THREAD_BLOCKED,     /* Waiting for an event to trigger. */
    THREAD_DYING        /* About to be destroyed. */
  };

/* Thread identifier type.
   You can redefine this to whatever type you like. */
typedef int tid_t;
/////////////////////////////////////////////////////////////////////////////////////////Start
typedef int pid_t;
/////////////////////////////////////////////////////////////////////////////////////////End
#define TID_ERROR ((tid_t) -1)          /* Error value for tid_t. */

/* Thread priorities. */
#define PRI_MIN 0                       /* Lowest priority. */
#define PRI_DEFAULT 31                  /* Default priority. */
#define PRI_MAX 63                      /* Highest priority. */

/* A kernel thread or user process.

   Each thread structure is stored in its own 4 kB page.  The
   thread structure itself sits at the very bottom of the page
   (at offset 0).  The rest of the page is reserved for the
   thread's kernel stack, which grows downward from the top of
   the page (at offset 4 kB).  Here's an illustration:

        4 kB +---------------------------------+
             |          kernel stack           |
             |                |                |
             |                |                |
             |                V                |
             |         grows downward          |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             +---------------------------------+
             |              magic              |
             |                :                |
             |                :                |
             |               name              |
             |              status             |
        0 kB +---------------------------------+

   The upshot of this is twofold:

      1. First, `struct thread' must not be allowed to grow too
         big.  If it does, then there will not be enough room for
         the kernel stack.  Our base `struct thread' is only a
         few bytes in size.  It probably should stay well under 1
         kB.

      2. Second, kernel stacks must not be allowed to grow too
         large.  If a stack overflows, it will corrupt the thread
         state.  Thus, kernel functions should not allocate large
         structures or arrays as non-static local variables.  Use
         dynamic allocation with malloc() or palloc_get_page()
         instead.

   The first symptom of either of these problems will probably be
   an assertion failure in thread_current(), which checks that
   the `magic' member of the running thread's `struct thread' is
   set to THREAD_MAGIC.  Stack overflow will normally change this
   value, triggering the assertion. */
/* The `elem' member has a dual purpose.  It can be an element in
   the run queue (thread.c), or it can be an element in a
   semaphore wait list (synch.c).  It can be used these two ways
   only because they are mutually exclusive: only a thread in the
   ready state is on the run queue, whereas only a thread in the
   blocked state is on a semaphore wait list. */
/////////////////////////////////////////////////////////////////////////////////////////start   
struct child
{
  tid_t id;
  int exit_code;
  struct list_elem child_elem;              /* List element. */
};
/////////////////////////////////////////////////////////////////////////////////////////end
struct thread
  {
    /* Owned by thread.c. */
    tid_t tid;                          /* Thread identifier. */
    enum thread_status status;          /* Thread state. */
    char name[16];                      /* Name (for debugging purposes). */
    uint8_t *stack;                     /* Saved stack pointer. */
    int priority;                       /* Priority. */
    struct list_elem allelem;           /* List element for all threads list. */
    /* Shared between thread.c and synch.c. */
    struct list_elem elem;              /* List element. */

#ifdef USERPROG
    /* Owned by userprog/process.c. */
    uint32_t *pagedir;                  /* Page directory. */
/////////////////////////////////////////////////////////////////////////////////////////Start
    pid_t pid;                          /*  Parent id*/
    struct list fdt;                    /*  File descriptor table*/
    unsigned file_no;
    struct file *executable;
/////////////////////////////////////////////////////////////////////////////////////////End

/////////////////////////////////////////////////////////////////////////////////////////start   
    struct list children;               /* Lista de copii -> de tipul struct child*/
    struct lock child_list_lock;        /* Lacat pentru lista de copii*/
    struct semaphore sema_exec;         /* Semafor pentru exec si wait*/
    struct semaphore sema_got_code;     /* Semafor ce asteapta ca parent sa ia load code de la child */
    struct semaphore sema_wait_exit;    /* Semfaor ce semnalizeaza terminarea unui child in caz ca parent asteapta*/    
    struct thread *parent;              /* Referinta la parinte*/
    int load_code;                      /* Load status */
    int exit_code;                      /* Cod de exit*/
/////////////////////////////////////////////////////////////////////////////////////////end
#endif

#ifdef VM
  struct hash supl_pt;
  struct file *exec_file;
  struct file **open_files;
#endif
    /* Owned by thread.c. */
    unsigned magic;                     /* Detects stack overflow. */
    //struct lock *child_list_lock;
  };

/* If false (default), use round-robin scheduler.
   If true, use multi-level feedback queue scheduler.
   Controlled by kernel command-line option "-o mlfqs". */
extern bool thread_mlfqs;

void thread_init (void);
void thread_start (void);

void thread_tick (void);
void thread_print_stats (void);

typedef void thread_func (void *aux);
tid_t thread_create (const char *name, int priority, thread_func *, void *);

void thread_block (void);
void thread_unblock (struct thread *);

struct thread *thread_current (void);
tid_t thread_tid (void);
const char *thread_name (void);

void thread_exit (void) NO_RETURN;
void thread_yield (void);

/* Performs some operation on thread t, given auxiliary data AUX. */
typedef void thread_action_func (struct thread *t, void *aux);
void thread_foreach (thread_action_func *, void *);

int thread_get_priority (void);
void thread_set_priority (int);

int thread_get_nice (void);
void thread_set_nice (int);
int thread_get_recent_cpu (void);
int thread_get_load_avg (void);
/////////////////////////////////////////////////////////////////////////////////////////start
struct thread* get_thread(int);
/////////////////////////////////////////////////////////////////////////////////////////end

/////////////////////////////////////////////////////////////////////////////////////////Start
inline struct file_descriptor* thread_get_file(int fd)
{
  struct file_descriptor *fde = NULL;
  struct list *fdt = &thread_current()->fdt;
  if(!list_empty(fdt))
  {
    struct list_elem *e;
    for (e = list_begin (fdt); e != list_end (fdt);
         e = list_next (e))
      {
        struct file_descriptor *f = list_entry (e, struct file_descriptor, elem);
        if(f->fd == fd)
        {
          fde = f;
          break;
        }
      }
  }
  return fde;
}
/////////////////////////////////////////////////////////////////////////////////////////End


#endif /* threads/thread.h */
