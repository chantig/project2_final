#include "frame.h"
#include <bitmap.h>
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/vaddr.h"
#include "threads/thread.h"
#include <stdio.h>
#include <string.h>
#include "swap.h"
#include "spte.h"

static struct frame_entry * frame_table;
static void * user_frames = NULL;

#define FRAME_FREE 0
#define FRAME_USED 1
static struct bitmap *free_frames_bitmap;
static struct lock frame_in_lock;
static struct lock frame_out_lock;
static size_t no_of_pages;

void * prepare_frame(struct supl_pte* spte);
void frame_evict(size_t idx);
void *frame_swap_in(struct supl_pte* spte);
size_t clock_algthm();
static size_t last_verified_frame; 
void frame_table_init(size_t number_of_user_pages)
{
	// allocate an array of frame entries
	frame_table = malloc(number_of_user_pages * sizeof(struct frame_entry));
	
	if(NULL == frame_table)
	{
		PANIC("Unable to allocate space for the frame table.");
	}
	last_verified_frame = 0;
	no_of_pages = number_of_user_pages;
	memset(frame_table, 0, number_of_user_pages * sizeof(struct frame_entry));
	user_frames = palloc_get_multiple(PAL_USER, number_of_user_pages);
	if(NULL == user_frames)
	{
		PANIC("Unable to claim user space for the frame manager.");
	}


	// initialize a bitmap to represent the free frames
	free_frames_bitmap = bitmap_create( number_of_user_pages);
	if(NULL == free_frames_bitmap)
	{
	    PANIC("Unable to initialize swap table!");
	}

	bitmap_set_all(free_frames_bitmap, FRAME_FREE);
	lock_init(&frame_in_lock);
	lock_init(&frame_out_lock);
}

// void *frame_alloc( enum palloc_flags flags,  struct supl_pte* spte)
// {
// 	ASSERT(0 != (flags & PAL_USER));
// 	ASSERT(NULL != frame_table);
// 	ASSERT(NULL != free_frames_bitmap);

// 	size_t free_idx = 0;

// 	free_idx = bitmap_scan_and_flip(free_frames_bitmap, 0, 1, FRAME_FREE);
// 	if(BITMAP_ERROR == free_idx)
// 	{
// 		PANIC("[frame_table] Table is full.");
// 	}

// 	frame_table[free_idx].spte = spte;
//     frame_table[free_idx].ownner_thread = thread_current();

//     if(0 != (PAL_ZERO & flags))
//     {
//     	memset((char *)user_frames + PGSIZE * free_idx, 0, PGSIZE);
//     }


//     return (char *)user_frames + PGSIZE * free_idx;
// }

void *frame_alloc( enum palloc_flags flags,  struct supl_pte* spte)
{
	ASSERT(0 != (flags & PAL_USER));
	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);

	char *free_frame = (char *)prepare_frame(spte);

    memset(free_frame, 0, PGSIZE);
    return free_frame;
}

void * prepare_frame(struct supl_pte* spte)
{
	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);

	size_t free_idx = 0;

	lock_acquire(&frame_in_lock);
	free_idx = bitmap_scan_and_flip(free_frames_bitmap, 0, 1, FRAME_FREE);

	if(BITMAP_ERROR == free_idx)
	{
		free_idx = clock_algthm();
		frame_evict(free_idx);
	}
	spte->frame_page_idx = free_idx;
    spte->swapped_out = false;
    frame_table[free_idx].last_time = timer_ticks();
	frame_table[free_idx].spte = spte;
	lock_release(&frame_in_lock);
    frame_table[free_idx].ownner_thread = thread_current();

	return (char *)user_frames + PGSIZE * free_idx;
}

void frame_evict(size_t idx)
{
	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);

	if (pagedir_is_dirty(frame_table[idx].ownner_thread->pagedir, frame_table[idx].spte->virt_page_addr) || 
		frame_table[idx].spte->writable)
	{
		size_t swap_idx = swap_out(user_frames + PGSIZE * idx);
		frame_table[idx].spte->swap_idx = swap_idx;
		frame_table[idx].spte->swapped_out = true;
	}
	pagedir_clear_page(frame_table[idx].ownner_thread->pagedir, frame_table[idx].spte->virt_page_addr);
}

void *frame_swap_in(struct supl_pte* spte)
{

	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);
	char *frame_to_write = (char *)prepare_frame(spte);
	swap_in(spte->swap_idx, frame_to_write);
	
	return frame_to_write;
}

void frame_free(void *frame_addr)
{
	ASSERT(frame_addr >=  user_frames);
	ASSERT(NULL != frame_table);
	ASSERT(NULL != free_frames_bitmap);

	size_t idx = ((size_t) frame_addr - (size_t)user_frames)/PGSIZE;

	bitmap_set(free_frames_bitmap, idx, FRAME_FREE);
}

size_t clock_algthm(){
	size_t idx_fnd = 0;
	while(true){
		if (frame_table[last_verified_frame].ownner_thread->pagedir != NULL)
		{
			if(pagedir_is_accessed(frame_table[last_verified_frame].ownner_thread->pagedir, frame_table[last_verified_frame].spte->virt_page_addr)){
				pagedir_set_accessed(frame_table[last_verified_frame].ownner_thread->pagedir, frame_table[last_verified_frame].spte->virt_page_addr, false);
						
			}else{
				goto found_it;
			}
		}else{

			goto found_it;
		}

		last_verified_frame = (last_verified_frame + 1) % no_of_pages; 
	}
	found_it:
	idx_fnd = last_verified_frame; 
	last_verified_frame = (last_verified_frame + 1) % no_of_pages; 
	return idx_fnd;
}
