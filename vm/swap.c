#include "swap.h"
#include <stdio.h>

struct bitmap * swap_table;
struct block  * swap_block;
static struct lock sw_lock; 
static struct lock uns_lock; 

void swap_init(void)
{
    block_sector_t swap_number_of_sectors = 0;
    swap_block = block_get_role(BLOCK_SWAP);
    if(NULL == swap_block)
    {
        PANIC("Unable to get the swap block!");
    }
    swap_number_of_sectors = block_size(swap_block);
    swap_table = bitmap_create( swap_number_of_sectors / SWAP_SECTORS_PER_PAGE);
    if(NULL == swap_table)
    {
        PANIC("Unable to initialize swap table!");
    }
    bitmap_set_all(swap_table, SWAP_FREE);
    lock_init(&sw_lock);
    lock_init(&uns_lock);

}


void swap_uninit(void)
{
    bitmap_destroy(swap_table);
}

void swap_in(size_t bitmap_idx, void *frame_addr)
{
    unsigned int i;

    ASSERT(NULL != swap_table);
    ASSERT(NULL != swap_block);
    ASSERT(bitmap_test(swap_table, bitmap_idx) == SWAP_USED);
    lock_acquire(&uns_lock);
    bitmap_scan_and_flip(swap_table,bitmap_idx,1,SWAP_USED);
    lock_release(&uns_lock);
    
    for (size_t i = bitmap_idx * 8; i < bitmap_idx * 8 + 8; i++)
    {
        block_read(swap_block,i,frame_addr + 512 * (i % 8));
    }
}

size_t swap_out(void *frame_addr)
{
    unsigned int i;
    size_t free_idx = 0;

    ASSERT(NULL != swap_table);
    ASSERT(NULL != swap_block);
    lock_acquire(&sw_lock); 
    free_idx = bitmap_scan_and_flip(swap_table,free_idx,1,SWAP_FREE);
    lock_release(&sw_lock);

    for (size_t i = free_idx * 8; i < free_idx * 8 + 8; i++)
    {
        block_write(swap_block,i,frame_addr + 512 * (i % 8));
    }
    return free_idx;
}

