#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H
#define BAD_ADDRESS -1


void syscall_init (void);
void sys_exit(int status);
#endif /* userprog/syscall.h */
