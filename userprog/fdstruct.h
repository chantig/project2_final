#ifndef USERPROG_BUILD_FDSTRUCT_H_
#define USERPROG_BUILD_FDSTRUCT_H_

#include <list.h>
#include "filesys/filesys.h"

struct file_descriptor
{
	int fd;
	struct file *file;
	struct list_elem elem;
};

#endif /* USERPROG_BUILD_FDSTRUCT_H_ */
