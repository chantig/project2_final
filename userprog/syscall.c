#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
//////////////////////////////////////////////////Start
#include "exitcodes.h"
#include "lib/kernel/console.h"
#include "devices/input.h"
#include "process.h"
#include "devices/timer.h"
#include "pagedir.h"
#include "threads/malloc.h"
#include "threads/vaddr.h"
#include "threads/pte.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "fdstruct.h"

#define MAX_PATH 1024
#define STACK (f->esp)
#define ARGS_OFFSET 4
#define ARGS_ADDRESS(stack) (stack + ARGS_OFFSET)

struct semaphore  filesys_lock = {0};
//////////////////////////////////////////////////End

static void syscall_handler (struct intr_frame *);

//////////////////////////////////////////////////Start


inline static pid_t 	sys_exec(struct intr_frame *f);
inline static int 		sys_wait(struct intr_frame *f);
inline static bool		sys_create(struct intr_frame *f);
inline static bool		sys_remove(struct intr_frame *f);
inline static int 		sys_open(struct intr_frame *f);
inline static int 		sys_filesize(struct intr_frame *f);
inline static int 		sys_write(struct intr_frame *f);
inline static int 		sys_read(struct intr_frame *f);
inline static void 		sys_seek(struct intr_frame *f);
inline static unsigned	sys_tell(struct intr_frame *f);
inline static void 		sys_close(struct intr_frame *f);
inline static int 		sys_ticks(struct intr_frame *f);

inline static void 		sys_read_args(const void *stack, int *buffer, int nr_args);
inline static void 		validate_addr(const void *);
inline static void 		validate_stack(const void *stack, int nr_args);
inline static void 		validate_s_buffer(const void*, unsigned);
inline static void 		validate_buff(const void*);

//////////////////////////////////////////////////End

void
syscall_init (void) 
{
//////////////////////////////////////////////////Start
  sema_init(&filesys_lock,1);
//////////////////////////////////////////////////End
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f ) 
{
  //validate stack pointer
	validate_addr(STACK);
	int syscall_no = ((int*) STACK)[0];
	switch (syscall_no)
	{
	case SYS_HALT:
		break;
	case SYS_EXIT:
	{
			int args[1];
			sys_read_args(ARGS_ADDRESS(STACK), args, 1);
			int status = args[0];
			sys_exit(status);
			break;
	}

	case SYS_EXEC:
	{
		f->eax = sys_exec(f);
		break;
	}
	case SYS_WAIT:
		f->eax = sys_wait(f);
		break;
	case SYS_CREATE:
		f->eax = sys_create(f);
		break;
	case SYS_REMOVE:
		f->eax = sys_remove(f);
		break;
	case SYS_OPEN:
		f->eax = sys_open(f);
		break;
	case SYS_FILESIZE:
		f->eax = sys_filesize(f);
		break;
	case SYS_READ:
		f->eax = sys_read(f);
		break;
	case SYS_WRITE:
		f->eax = sys_write(f);
		break;
	case SYS_SEEK:
		sys_seek(f);
		break;
	case SYS_TELL:
		f->eax = sys_tell(f);
		break;
	case SYS_CLOSE:
		sys_close(f);
		break;
	case SYS_TEST:
		//printf("SYS_TEST system call!\n");
		break;
	case SYS_TICKS:
		//printf("SYS_TICKS system call!\n");
		f->eax = sys_ticks(f);
		break;
	default: ;
	}
	return;
}

inline void sys_exit(int status)
{
	struct thread *t = thread_current();
//////////////////////////////////////////////////////////////////////////////start
	t->exit_code = status;
	printf("%s: exit(%d)\n", t->name, status);
	thread_exit();
//////////////////////////////////////////////////////////////////////////////end	
}

inline static pid_t sys_exec(struct intr_frame *f)
{
	int args[1];
	sys_read_args(ARGS_ADDRESS(STACK), args, 1);

	const void *filename = (char*)args[0];
	validate_buff(filename);

//////////////////////////////////////////////////////////////////////////////start
	
	return(process_execute(filename));
	
//////////////////////////////////////////////////////////////////////////////end	
}

inline static int sys_wait(struct intr_frame *f)
{
	int args[1];
	sys_read_args(ARGS_ADDRESS(STACK), args, 1);
	pid_t pid = args[0];

//////////////////////////////////////////////////////////////////////////////start
	
	return process_wait(pid);

//////////////////////////////////////////////////////////////////////////////end
}

inline static bool sys_create(struct intr_frame *f)
{
	int args[2];
	sys_read_args(ARGS_ADDRESS(STACK), args, 2);

	const char *fname = (char *)args[0];
	unsigned initial_size = args[1];
	validate_buff(fname);

	sema_down(&filesys_lock);
	bool status = filesys_create(fname, initial_size);
	sema_up(&filesys_lock);

	return status;
}

inline static bool sys_remove(struct intr_frame *f)
{
	int args[1];
	sys_read_args(ARGS_ADDRESS(STACK), args, 1);

	const char *fname = (char *)args[0];
	validate_buff(fname);

	sema_down(&filesys_lock);
	bool status = filesys_remove(fname);
	sema_up(&filesys_lock);

	return status;
}

inline static int sys_open(struct intr_frame *f)
{
	int args[1];
	sys_read_args(ARGS_ADDRESS(STACK), args, 1);

	const void *fname = (void *) args[0];
	validate_buff(fname);

	sema_down(&filesys_lock);
	struct file *file = filesys_open(fname);
	sema_up(&filesys_lock);

	if(NULL == file) return -1;

	struct file_descriptor *fd = (struct file_descriptor*)malloc(sizeof(struct file_descriptor));
	struct thread *t = thread_current();
	fd->fd = t->file_no++;
	fd->file = file;

	list_push_back(&t->fdt, &fd->elem);

	return fd->fd;
}

inline static int sys_filesize(struct intr_frame *f)
{
	int args[1];
	sys_read_args(ARGS_ADDRESS(STACK), args, 1);

	int fd = args[0];
	struct file_descriptor *fde = thread_get_file(fd);
	if(NULL == fde) return -1;

	sema_down(&filesys_lock);
	int size = file_length(fde->file);
	sema_up(&filesys_lock);

	return size;
}

inline static int sys_read(struct intr_frame *f)
{
	int args[3];
	sys_read_args(ARGS_ADDRESS(STACK), args, 3);

	int fd = args[0];
	const void *buffer = (void *) args[1];
	unsigned size = args[2];

	if(size == 0) return 0;
	validate_s_buffer(buffer, size);

	unsigned read_bytes = 0;

	if (fd == STDIN_FILENO)
	{
		sema_down(&filesys_lock); //-------------------------------------------------------------------------????trebuie?
		while (read_bytes < size)
		{
			((uint8_t*) buffer)[read_bytes++] = input_getc();
		}
		sema_up(&filesys_lock);
	}
	else
	{
		struct file_descriptor *fde = thread_get_file(fd);
		if(fde == NULL) return -1;

		sema_down(&filesys_lock);
		read_bytes = file_read(fde->file, buffer, size);
		sema_up(&filesys_lock);
	}
	return read_bytes;
}

inline static int sys_write(struct intr_frame *f)
{
	int args[3];
	sys_read_args(ARGS_ADDRESS(STACK), args, 3);

	int fd = args[0];
	const void *buffer = (void *) args[1];
	unsigned size = args[2];

	if(size == 0) return 0;
	validate_s_buffer(buffer, size);

	int written_bytes = size;
	if (fd == STDOUT_FILENO)
	{
		putbuf(buffer, size);
	}
	else
	{
		struct file_descriptor *fde = thread_get_file(fd);
		if(fde == NULL) return -1;

		sema_down(&filesys_lock);
		written_bytes = file_write(fde->file, buffer, size);
		sema_up(&filesys_lock);
	}
	return written_bytes;
}

inline static void sys_seek(struct intr_frame *f)
{
	int args[2];
	sys_read_args(ARGS_ADDRESS(STACK), args, 2);
	int fd = args[0];
	unsigned position = args[1];

	struct file_descriptor *fde = thread_get_file(fd);
	if(NULL == fde) return;

	sema_down(&filesys_lock);
	file_seek(fde->file, position);
	sema_up(&filesys_lock);
}

inline static unsigned	sys_tell(struct intr_frame *f)
{
	int args[1];
	sys_read_args(ARGS_ADDRESS(STACK), args, 1);
	int fd = args[0];

	struct file_descriptor *fde = thread_get_file(fd);
	if(NULL == fde) return -1;

	sema_down(&filesys_lock);
	unsigned pos = file_tell(fde->file);
	sema_up(&filesys_lock);

	return pos;
}

inline static void sys_close(struct intr_frame *f)
{
	int args[1];
	sys_read_args(ARGS_ADDRESS(STACK), args, 1);
	int fd = args[0];

	struct file_descriptor *fde = thread_get_file(fd);
	if(NULL == fde) return;

	sema_down(&filesys_lock);
	file_close(fde->file);
	sema_up(&filesys_lock);

	list_remove(&fde->elem);
	free(fde);
}

inline static int sys_ticks(struct intr_frame *f UNUSED)
{
	return timer_ticks();
}

inline static void sys_read_args(const void *stack, int *args, int nr_args)
{
	validate_stack(stack, nr_args);
	int *istack = (int *) stack;
	for (int i = 0; i < nr_args; i++)
		args[i] = istack[i];

}

static void validate_addr(const void *address)
{
	if ( NULL == address ) sys_exit(BAD_ADDRESS);
	if ( !is_user_vaddr(address) ) sys_exit(BAD_ADDRESS);
	uint32_t *pde, *pt;
	pde = thread_current()->pagedir + pd_no(address);
	if (! (*pde & PTE_P))
	{
		sys_exit(BAD_ADDRESS);
	}
	pt = pde_get_pt(*pde);
	if (! (pt[pt_no(address)] & PTE_P))
	{
		sys_exit(BAD_ADDRESS);
	}

}

static void validate_stack(const void *stack, int nr_args)
{
	int *_stack = (int*) stack;
	for (int arg = 0; arg < nr_args; arg++)
	 validate_addr(_stack + arg);
}

static void validate_s_buffer(const void *buffer, unsigned size)
{
	char *_buffer = (char *) buffer;
	for (unsigned i = 0; i < size; i++) validate_addr(_buffer + i);
}

static void validate_buff(const void* buffer)
{
	char *_buffer = (char*)buffer;
	int size = 0;
	while(size < MAX_PATH)
	{
		validate_addr(_buffer + size);
		if(_buffer[size++] == '\0') break;
	}
}