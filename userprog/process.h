#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"
#ifdef VM
#include <stdint.h>
#include "filesys/file.h"
#include "vm/spte.h"
#endif

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);
struct argv_s
{
	char *point_to;
	struct list_elem elem;
};

#define MAX_FILE_NAME 16
#ifdef VM
bool load_page_for_address(uint8_t *upage);
bool lazy_loading_page_for_address(	struct supl_pte *spte, void *upage);
#endif

#endif /* userprog/process.h */
